# dotfiles

Clone and/or update submodules.
```
git submodule update --init --recursive
```

```
ln -s ~/dotfiles/bashrc ~/.bashrc
ln -s ~/dotfiles/bash_aliases ~/.bash_aliases
ln -s ~/dotfiles/tmux.conf ~/.tmux.conf
ln -s ~/dotfiles/gitconfig ~/.gitconfig
ln -s ~/dotfiles/vimrc ~/.vimrc
ln -s ~/dotfiles/vim ~/.vim
```

tmux-256color in MacOS ([link](https://gist.github.com/bbqtd/a4ac060d6f6b9ea6fe3aabe735aa9d95)):
```
brew install ncurses
/usr/local/opt/ncurses/bin/infocmp tmux-256color > ~/tmux-256color.info
tic -xe tmux-256color tmux-256color.info

# This creates a complied entry in ~/.terminfo
infocmp tmux-256color | head
#       Reconstructed via infocmp from file: /Users/libin/.terminfo/74/tmux-256color
#  tmux-256color|tmux with 256 colors,
```

Remap CapsLock to Ctrl:
```
# /etc/default/keyboard
XKBOPTIONS="caps:ctrl_modifier"
```

Build vim from source:

```
# git clone https://github.com/vim/vim

sudo apt-get install libncurses5-dev libgnome2-dev libgnomeui-dev \
    libgtk2.0-dev libatk1.0-dev libbonoboui2-dev \
    libcairo2-dev libx11-dev libxpm-dev libxt-dev python-dev ruby-dev

sudo apt-get remove vim vim-runtime gvim vim-tiny vim-common vim-gui-common

./configure --with-features=huge \
            --disable-darwin \
            --disable-selinux \
            --enable-luainterp \
            --enable-perlinterp \
            --enable-pythoninterp \
            --enable-python3interp \
            --enable-tclinterp \
            --enable-rubyinterp \
            --enable-cscope \
            --enable-multibyte \
            --enable-xim \
            --enable-fontset \
            --enable-gui=gnome2
make
sudo apt-get install checkinstall
sudo checkinstall

sudo update-alternatives --install /usr/bin/editor editor /usr/local/bin/vim 1
sudo update-alternatives --set editor /usr/local/bin/vim
```
